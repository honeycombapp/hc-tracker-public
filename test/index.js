const assert = require("assert");
const Tracker = require("../index.js");
const Event = require("../src/Event.js");
const COLUMN_NAMES = require("../src/constants.js").COLUMN_NAMES;

// Add tests here as they are written
const tests = [setupTest, runningTest, sessionIDTest,
    codesImportTest, emptyQueueTest, trackingTest,
    automaticSendingTest, emptyEventTest, validEventTest, createAndSerializeEventTest];

runTests();

/**
 * Helper function to test if objects are identical in keys and values
 * @param {Object} a 
 * @param {Object} b 
 */
function isObjectEquals(a, b) {
    let aProps = Object.getOwnPropertyNames(a);
    let bProps = Object.getOwnPropertyNames(b);

    if (aProps.length != bProps.length) {
        return false;
    }

    for (let i = 0; i < aProps.length; i++) {
        let propName = aProps[i];

        if (a[propName] !== b[propName]) {
            return false;
        }
    }

    return true;
}

function runTests(){
    //Persistent variable across tests
    let tracker;
    for(let i = 0; i < tests.length; i++) {
        console.log("\nRunning test #" + i + ": " + tests[i].name);
        let result = tests[i]();
        assert(result);
        console.log("\nTest " + i.toString() + ' passed');
    }
    console.log("All tests passed successfully!")
    process.exit();    
}

function setupTest(){
    console.log("Ensuring Tracker was imported properly..");
    assert(Tracker!=undefined);
    tracker = new Tracker();
    return(tracker!=undefined);
}

function runningTest(){
    console.log("Ensuring tracker is running properly..");
    let running = tracker.run();
    return running;
}

function sessionIDTest(){
    console.log("Ensuring session id is set up properly..");
    return(tracker.getSessionID() != null);
}

function codesImportTest(){
    console.log("Ensuring source and action codes are able to be returned..");
    let sources = tracker.getSourceCodes();
    let actions = tracker.getActionCodes();
    assert(sources && sources["APP_SOURCE"] == 1);
    return(actions && actions["MENU_VIEW_MOB"] == 101);
}

function emptyQueueTest(){
    console.log("Ensuring empty queue returns null..");
    return(tracker.peek() == null);
}

/**
 * Test if the tracker correctly tracks a new event
 */
function trackingTest() {
    console.log("Ensuring that events are tracked..");
    
    // Set up event data
    let sessionID = tracker.getSessionID();
    let source = 1;
    let action = 2;
    let data = {};
    data[COLUMN_NAMES.user_id] = '5';
    data[COLUMN_NAMES.restaurant_id] = 12;
    data[COLUMN_NAMES.data] = "Menu Clicked";

    tracker.trackEvent(source, action, data);
    
    return (isObjectEquals(tracker.peek(), new Event(sessionID, source, action, data)));
}

// TODO: Fix test
// for now return true if tracker did not return
function automaticSendingTest(){
    console.log("Ensure tracker can track multiple events..");
    // Set up event data
    let sessionID = tracker.getSessionID();
    let source = 1;
    let action = 2;
    let data = {};
    data[COLUMN_NAMES.user_id] = '5';
    data[COLUMN_NAMES.restaurant_id] = 12;
    data[COLUMN_NAMES.data] = "Menu Clicked";

    for(let i = 0; i<8; i++){
        tracker.trackEvent(source, action, data); 
    }
    return true;
}

/**
 * Empty the tracking queue and attempt to add various empty events.
 * Expects an empty queue despite all the attempted event adds.
 * 
 * This test does not need to cover undefined and null data values. For
 * that, @see createAndSerializeEventTest.
 */
function emptyEventTest() {
    console.log("Ensure queue does not add invalid events");
    tracker.emptyQueue();
    tracker.trackEvent(null, null, null);
    tracker.trackEvent(1, null, {});
    tracker.trackEvent(null, 2, {});
    tracker.trackEvent(null, null, {});
    tracker.trackEvent(1, 2, {});
    tracker.trackEvent(1, 2, {'fake key':'fake value'});

    return (emptyQueueTest());
}

/**
 * Validate an event
 */
function validEventTest(){
    console.log('Ensure an event created is valid');

    // Create fake raw data
    let sessionID = tracker.getSessionID();
    let source = 1;
    let action = 2;
    let restaurant_id = 12;
    let data = {};
    data[COLUMN_NAMES.restaurant_id] = restaurant_id;
    data[COLUMN_NAMES.user_id] = null;
    data[COLUMN_NAMES.data] = undefined;
    data["Fake Key"] = "Fake Value";
    
    let event = new Event(sessionID, source, action, data);
    
    let manualEvent = {};
    manualEvent[COLUMN_NAMES.session_id] = sessionID;
    manualEvent[COLUMN_NAMES.source] = source;
    manualEvent[COLUMN_NAMES.action] = action;
    manualEvent[COLUMN_NAMES.restaurant_id] = restaurant_id;

    return isObjectEquals(manualEvent, event);
}

/**
 * Tests if event creation and serialization works for valid events
 * but whose fields do not conform with the database specification
 * 
 * Expects an event that has valid keys and values that are defined and not null
 */
function createAndSerializeEventTest(){
    console.log('Ensure events created and serialized are valid');

    // Create fake raw data
    let sessionID = tracker.getSessionID();
    let source = 1;
    let action = 2;
    let data = {};
    data[COLUMN_NAMES.restaurant_id] = 12;
    data[COLUMN_NAMES.user_id] = null;
    data[COLUMN_NAMES.data] = undefined;
    data["Fake Key"] = "Fake Value";
    
    let preprocessedEvent = new Event(sessionID, source, action, data);
    
    // Create expected data
    let postSessionID = tracker.getSessionID();
    let postSource = 1;
    let postAction = 2;
    let postData = {};
    postData[COLUMN_NAMES.restaurant_id] = 12;

    let postprocessedEvent = new Event(postSessionID, postSource, postAction, postData);

    return ( 
        isObjectEquals(
            preprocessedEvent.serializeEvent(),
            postprocessedEvent
        )
    );
}