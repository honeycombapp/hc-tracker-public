# Honey Tracker
* NPM module that tracks events across the Honeycomb System and sends to an analytics backend
* Repository for the javascript universal Honeycomb Tracker
* Built as npm module to work across everything in the current system

## Requirements
```
node.js > 6.0
npm > 5.0
```

## Setup
`npm install`

## Test
`npm test`

## Usage
`import Tracker from {PATH_TO_INDEX}` 
or `var Tracker = require("PATH_TO_INDEX)`

``` JavaScript
    tracker = new Tracker();
    tracker.run();
    tracker.trackEvent(SOURCE, EVENT, DATA);
```
* `SOURCE` refers to a number representing the module being tracked 
* `EVENT` refers to a number representing the event being tracked
* `DATA` is additional supplementary data to be provided with an event
* `Tracker` objects have the encodings available via the getActionCodes and getSourceCodes methods
alternatively, they are also present in `src/encodings.js`.

### Setting Defaults
* To change the time frequency of checking the event queue or modifying the max number of events in
the queue, prior to running the tracker you can set defaults as follows

``` 
    // debug = whether debug flag should be true or false
    tracker.setDefaults(debug, maxQueueTime, maxQueueEvents)
```
