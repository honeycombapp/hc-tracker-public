'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var uniqueID = require('shortid');
var axios = require('axios');

var Event = require("./src/Event");
var CODES = require("./src/encodings");
var BASE_URL = require("./src/constants").BASE_URL;

/**
 * Base class module to handle tracking of events.
 * Contains a queue of elements
 */

var Tracker = function () {
    function Tracker(sessionID) {
        _classCallCheck(this, Tracker);

        this.debug = false;
        //Max interval to cheque queue in seconds
        this.maxQueueTime = 10;
        //Max number of events before queue fires
        this.maxQueueEvents = 10;
        //initialize empty queue
        this.queue = [];

        //create a session id for this tracker
        this.sessionID = sessionID ? sessionID : uniqueID();
    }

    /**
     * Runs server and checks queue every maxQueueTime seconds
     */


    _createClass(Tracker, [{
        key: 'run',
        value: function run() {
            this.interval = setInterval(this.checkQueue.bind(this), this.maxQueueTime * 1000);
            return true;
        }

        /**
         * Configures parameters of tracker
         * @param {boolean} debug : denotes whether or not in debug mode 
         * @param {int} maxQueueTime : denotes the max time in seconds that the queue should send events remotely
         * @param {int} maxQueueEvents : denotes the max number of events allowed in the queue
         */

    }, {
        key: 'setDefaults',
        value: function setDefaults(debug, maxQueueTime, maxQueueEvents) {
            this.debug = debug ? debug : this.debug;
            this.maxQueueTime = maxQueueTime ? maxQueueTime : this.maxQueueTime;
            this.maxQueueEvents = maxQueueEvents ? maxQueueEvents : this.maxQueueEvents;
        }

        /**
         * Periodically checks the event queue and pushes to database if not empty
         */

    }, {
        key: 'checkQueue',
        value: function checkQueue() {
            if (this.queue.length > 0) {
                if (this.debug) console.log("Not empty!", this.queue);
                this.sendEvents();
            } else {
                if (this.debug) console.log("Queue Empty!");
            }
        }

        /**
         * Return last element in queue without
         * @return last event as json string
         */

    }, {
        key: 'peek',
        value: function peek() {
            if (!this.queue) return null;
            return this.queue[this.queue.length - 1];
        }

        /**
         * @param {int} source : source id of event. 
         * Must be one of sourceCodes in CODES
         * @param {int} action : action id of event. 
         * Must be one of actionCodes in CODES 
         * @param {obj} dataFields : data in addition to the event 
         * Documented in CODES under actionCodes
         */

    }, {
        key: 'trackEvent',
        value: function trackEvent(source, action, dataFields) {
            var event = new Event(this.sessionID, source, action, dataFields, this.debug);

            // Only add to queue if event is valid
            if (Object.keys(event).length > 0) {
                var eventJSON = event.serializeEvent();
                if (this.debug) console.log("Event: ", eventJSON);
                this.queue.push(eventJSON);
            }

            if (this.debug) console.log(this.queue);

            if (this.queue.length >= this.maxQueueEvents) {
                this.checkQueue();
            }
        }

        /**
         * Sends request to server to send the current elements in the queue
         */

    }, {
        key: 'sendEvents',
        value: function sendEvents() {
            var data = this.queue;
            var that = this;
            var promise = new Promise(function (resolve, reject) {
                axios.post(BASE_URL, data, {
                    headers: {
                        "Content-Type": 'application/json'
                    }
                }).then(function (response) {
                    resolve();
                    that.emptyQueue();
                }).catch(function (err) {
                    reject();
                    if (that.debug) console.log(err);
                });
            });
            return promise;
        }

        /**
         * @return encodings for sources
         */

    }, {
        key: 'getSourceCodes',
        value: function getSourceCodes() {
            return CODES.sources;
        }

        /**
         * @return encoding for actions
         */

    }, {
        key: 'getActionCodes',
        value: function getActionCodes() {
            return CODES.actions;
        }
    }, {
        key: 'getSessionID',
        value: function getSessionID() {
            return this.sessionID;
        }
    }, {
        key: 'emptyQueue',
        value: function emptyQueue() {
            this.queue = [];
        }
    }]);

    return Tracker;
}();

module.exports = Tracker;