module.exports = {
    // Module type where this event has been tracked
    sources: {
        APP_SOURCE: 1,
        WEB_SOURCE: 2,
    },
    actions: {
        /* Code of event types. 
        * Beside each event is an indication of additional data of that type
        * to be specified in data field
        */

        //WEB EVENTS
        MENU_VIEW_EVENT:      1,    //None 
        DIET_CLICK_EVENT:     2,    //Diet Name
        ALLERGEN_CLICK_EVENT: 3,    //Allergen Name
        ITEM_LIKE_EVENT:      4,    //Item Name
        MENU_CHANGE_EVENT:    5,    //Menu Name
        FILTER_CLICK_EVENT:   6,    //None
        ORDER_CLICK_EVENT:    7,    //None
        INFO_CLICK_EVENT:     8,    //None

        //MOBILE EVENTS
        MENU_VIEW_MOB        : 101,    //Restaurant ID *
        ITEM_VIEW            : 102,    //Item _id * 
        MAP_VIEW             : 103,    //None x
        LIST_VIEW            : 104,    //None x
        FAV_VIEW             : 105,    //None *
        PROF_VIEW            : 106,    //None *
        DIET_UPDATE          : 107,    //Diet Name (string) *
        BIN_UPDATE           : 108,    //Diet Vector (int) *
        RESTAURANT_SEARCH    : 109,    //Search Term (string), location (point) *
        ITEM_SEARCH          : 110,    //Search Term(string), location(point) *
        HIVE_VIEW            : 111,    //None *
        EAT_VIEW             : 112,    //None *
        REVIEWS_VIEW         : 113,    //Restaurant ID*
        POLICY_VIEW          : 114,    //Restaurant ID *
        DIRECTIONS_ACTION    : 115,    //Restaurant ID *
        CALL_ACTION          : 116,    //Restaurant ID *
        DELIVERY_ACTION      : 117,    //Restaurant ID *
        PICKUP_ACTION        : 118,    //Restaurant ID 
        RESERVE_ACTION       : 119,    //Restaurant ID *
        USER_VIEW            : 120,    // User name
        PLACE_SHORTLIST      : 121,    //Restaurant ID
        RECOMMENDED_ITEM     : 122,    //Restaurant ID, Item Name
        RECOMMENDED_PLACE    : 123,    //Restaurant ID
        REVIEW_VOTE          : 124,    //Review ID, 'up'/'down'
        PLACE_VOTE           : 125,    //Restaurant ID, 'up'/'down'
        ITEM_VOTE            : 126,    //Item ID, 'up'/'down' * 
        UNVERIFIED_CONFIRM   : 127,    //Restaurant ID, Item Name
        UNVERIFIED_REJECT    : 128, 
        REVIEW_START         : 129,    //Restaurant ID
        REVIEW_COMPLETE      : 130,    //Restaurant ID
        GROUP_CREATE         : 131,    //Group Name
        GROUP_EXPLORE        : 132, 
        FRIEND_ADD           : 133,    //User Name
        MAP_RESTAURANT_CLICK : 134,    //Restaurant ID 
        
    }
}
