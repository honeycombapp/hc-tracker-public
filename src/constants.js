module.exports = {
    BASE_URL : "https://honey-analytics-production.herokuapp.com/track",

    // Columns in the back end tracking table. This is
    // identical to COLUMN_NAMES in the back end helper.js
    COLUMN_NAMES: {
        "session_id": "session_id",
        "source": "source",
        "action": "action",
        "restaurant_id": "restaurant_id",
        "sub_source": "sub_source",
        "ip": "ip",
        "user_id": "user_id",
        "user_binary": "user_binary",
        "data": "data",
        "enriched": "enriched",
        "country": "country",
        "city": "city",
        "coord": "coord",
    }
}