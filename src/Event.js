"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var COLUMN_NAMES = require("./constants").COLUMN_NAMES;

/**
 * Base class module to handle events
 */

var Event = function () {

    /**
     * Fields required to construct an event
     * @param {string} sessionID REQUIRED
     * @param {int} source REQUIRED
     * @param {int} actionCode REQUIRED: Key for action
     * @param {obj} dataFields REQUIRED: additional data or null
     * @param {bool} debug is in debug mode 
     */
    function Event(sessionID, source, actionCode, dataFields, debug) {
        _classCallCheck(this, Event);

        if (!sessionID || !source || !actionCode) return null;
        if (Object.keys(dataFields).length === 0) return null;

        this[COLUMN_NAMES.session_id] = sessionID;
        this[COLUMN_NAMES.source] = source;
        this[COLUMN_NAMES.action] = actionCode;

        if (debug) console.log("Source: ", this[COLUMN_NAMES.source], " Action: ", this[COLUMN_NAMES.action]);

        for (var field in dataFields) {
            if (field in COLUMN_NAMES) {
                if (dataFields[field] != null) {
                    this[field] = dataFields[field];
                }
            }
        }
    }

    /**
     * Consolidates all events of this class into a javascript string
     * @return JSON of the data from this event
     */


    _createClass(Event, [{
        key: "serializeEvent",
        value: function serializeEvent() {
            for (var key in this) {
                if (this[key] == null) delete this[key];
            }

            return this;
        }
    }]);

    return Event;
}();

module.exports = Event;